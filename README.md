# HFE - Sopel modules

Custom modules for the [Sopel IRC bot](https://github.com/sopel-irc/sopel), dedicated to the #hfe channel at GameSurge.
Place the files at ~/.sopel/modules

## Modules

- **roomlist.py** - Print the [Hero Fighter](http://herofighter.com/) game room list.
- **rl-link.py** - Print URL for seeing Room List online.
- **download-link.py** - Print URL for downloading HF and Room Server.
