import sopel.module

from random import randint
import urllib
from xml.dom import minidom

@sopel.module.commands('roomlist', 'rl')
def roomlist(bot, trigger):
    try:
        f = urllib.urlopen('http://herofighter.com/hf/rlg.php?ver=700&cc=de&s=' + str(randint(0, 99999)));
        data = f.read();
        f.close();
    except IOError:
        bot.say('Failed to connect to the server.');
        quit();
    data = minidom.parseString(data);
    rooms = data.getElementsByTagName('room');
    if rooms.length <= 0:
        bot.say('No rooms available.');
        quit();
    i = 1;
    msgfun = lambda l: bot.msg(trigger.nick, l);
    bot.reply('The reply for this command is too long; I\'m sending it to you in a private message.')
    for room in rooms:
        room_s = ( str(i) + '. \x02' + room.getElementsByTagName("rn")[0].firstChild.nodeValue + '\x0f' +
                   '\t' + room.getElementsByTagName("dc")[0].firstChild.nodeValue +
                   '\t' + room.getElementsByTagName("cc")[0].firstChild.nodeValue +
                   '\t' + room.getElementsByTagName("n")[0].firstChild.nodeValue +
                   '/' + room.getElementsByTagName("nl")[0].firstChild.nodeValue );
        ppl = room.getElementsByTagName("ppl");
        if ppl[0].firstChild != None:
           room_s += '\t' + ppl[0].firstChild.nodeValue;
        msgfun(room_s);
        i+=1;